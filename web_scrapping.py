# !pip install BeautifulSoup4

import requests
from bs4 import BeautifulSoup
import pandas as pd

product_description = []
product_price = []
relative_url = []
gtin = []
product_links = []
url_photo = []

# Função para buscar url da photo do produto
def get_url_photo(turl, classe):
        response = requests.get(turl)
        data = response.text
        soup = BeautifulSoup(data, 'html.parser')
        return soup.find('a', {'class': classe}).get('href')

# Função para buscar código do produto (SKU)
def get_sku(url):
    response = requests.get(url)
    data = response.text
    soup = BeautifulSoup(data, 'html.parser')
    return soup.find('div', {'class': 'skuReference'}).get_text()

# Função para buscar código único do produto (GTIN)
def get_gtin(url):
    k = requests.get(url)
    obj = k.json()
    return obj[0]['items'][0]['ean']

for x in range(1, 7):
    hdr = headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) ' 
                            'AppleWebKit/537.11 (KHTML, like Gecko) '
                            'Chrome/23.0.1271.64 Safari/537.11',
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
                            'Accept-Encoding': 'none',
                            'Accept-Language': 'en-US,en;q=0.8',
                            'Connection': 'keep-alive'}

    url = 'https://www.comper.com.br/buscapagina?fq=C:/1967/&PS=32&sl=8c91f63c-18aa-4c65-a2da-7ee53dbab287&cc=32&sm=0&PageNumber=' + str(x)
    # Salvar em arquivo TXT URL lidas
    url_pagination_file = open('data_web_scrapping/url_pagination_file.txt', 'a')
    url_pagination_file.write(url + '\n')
    url_pagination_file.close()

    k = requests.get(url, headers=hdr)

    soup = BeautifulSoup(k.content,'html.parser')

    product_list = soup.find_all("li",{"class":"hortifruti"})


    for product in product_list:
        product_links.append(product.find("div",{"class":"shelf-item"}))


for result in product_links:
    
    try:
        # DESCRICAO
        product_description.append(result.find('a', {'class': 'shelf-item__title-link'}).get_text())
    except:
        product_description.append('n/a')

    try:
        # PRECO
        product_price.append(result.find('strong').get_text())
    except:
        product_price.append('n/a')

    try:
        # URL
        relative_url.append(result.find('a', {'class':'shelf-item__title-link'}).get('href'))
    except:
        relative_url.append('n/a')

for i in relative_url:
            try:
                # URL_PHOTO
                url_photo.append(get_url_photo(i, 'image-zoom'))
            except:
                url_photo.append('n/a')
            try:
                url_json = 'https://www.comper.com.br/api/catalog_system/pub/products/search/?fq=skuId:' + get_sku(i)
                # GTIN
                gtin.append(get_gtin(url_json))
            except:
                gtin.append('n/a')


df = pd.DataFrame({'GTIN': gtin, 'Descrição': product_description, 'Preço': product_price, 'URL': relative_url, 'URL_PHOTO': url_photo})

df.to_csv('data_web_scrapping/web_scrapping.csv', header=['GTIN', 'DESCRICAO', 'PRECO', 'URL', 'URL_PHOTO'], index=False)